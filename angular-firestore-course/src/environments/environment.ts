// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD70MAo_cuqhbOhbkzRvh1j0YOG4KhOodA",
    authDomain: "angularfirestorecourse.firebaseapp.com",
    databaseURL: "https://angularfirestorecourse.firebaseio.com",
    projectId: "angularfirestorecourse",
    storageBucket: "angularfirestorecourse.appspot.com",
    messagingSenderId: "698745516770",
    appId: "1:698745516770:web:2bfc3da09755c2f6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
