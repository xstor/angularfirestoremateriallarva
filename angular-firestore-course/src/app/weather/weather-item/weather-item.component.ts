import {Component, OnInit} from '@angular/core';
import {Weather} from '../../shared/interfaces/weather';
import {WeatherData} from '../../shared/interfaces/weather.data';
import {WeatherDataService} from '../weather-data.service';
import {AuthServiceService} from "../../user/auth-service.service";
import {FirebaseService} from "../../shared/firebase.service";
import {MatSnackBar} from "@angular/material";
import {User} from "../../shared/interfaces/user";
import {City} from "../../shared/interfaces/city";

@Component({
  selector: 'app-weather-item',
  templateUrl: './weather-item.component.html',
  styleUrls: ['./weather-item.component.scss']
})
export class WeatherItemComponent implements OnInit {
  get weather(): Weather {
    return this.weatherDataService.weather;
  }

  user$: User;

  constructor(private weatherDataService: WeatherDataService, private authService: AuthServiceService, private firebaseService: FirebaseService, private snackBar: MatSnackBar) {
    this.authService.user$.subscribe(user => this.user$ = user);
  }

  addCity() {
    const city: City = {
      name: this.weather.name,
      country: this.weather.country,
      description: this.weather.description,
      temperature: this.weather.temperature,
      lat: this.weather.lat,
      lon: this.weather.lon
    };
    this.firebaseService.addCity(this.user$.uid, city).then(res => {
      this.snackBar.open('Succes! City Saved', 'OK', {duration: 5000})
    }).catch(error => {
      console.log(error);
    });
  }

  ngOnInit() {
  }


}
