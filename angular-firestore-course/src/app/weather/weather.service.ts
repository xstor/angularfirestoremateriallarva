import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {WeatherData} from '../shared/interfaces/weather.data';
import {Weather} from '../shared/interfaces/weather';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private URL = 'https://api.openweathermap.org/data/2.5/weather?q=';
  private KEY = 'c2cd6a6967299374cc9f47ff371c486f';
  private IMP = '&units=imperial';

  constructor(private http: HttpClient) {
  }

  private handleError(res: HttpErrorResponse) {
    console.error(res);
    return throwError(res.error || 'Server error');
  }

  private transformWeatherData(data: WeatherData): Weather {
    return {
      name: data.name,
      country: data.sys.country,
      image: 'https://openweathermap.org/img/w' + data.weather[0].icon + '.png',
      temperature: data.main.temp,
      description: data.weather[0].description,
      lat: data.coord.lat,
      lon: data.coord.lon
    };
  }

  searchWeatherData(cityName: string): Observable<Weather> {
    return this.http.get<WeatherData>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      tap(data => console.log(JSON.stringify(data)),
        catchError(this.handleError)));
  }
}
