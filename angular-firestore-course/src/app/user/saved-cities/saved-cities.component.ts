import {Component, OnInit} from '@angular/core';
import {City} from '../../shared/interfaces/city';
import {FirebaseService} from "../../shared/firebase.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-saved-cities',
  templateUrl: './saved-cities.component.html',
  styleUrls: ['./saved-cities.component.scss']
})
export class SavedCitiesComponent implements OnInit {
  public cities: any[];
  city: any = {};
  public panelOpenState: boolean;
  updateForm = false;
  saveForm = true;
  userId = this.route.snapshot.paramMap.get('id');

  constructor(private firebaseService: FirebaseService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    const param = this.route.snapshot.paramMap.get('id');
    if (param) {
      console.log(param);
      this.getCities(param);
    }
  }

  getCities(id: string) {
    this.firebaseService.getUserCities(id).subscribe(data => {
      console.log('New City');
      this.cities = data;
    });
  }

  update(city: any) {
    this.city = city;
  }

  delete(city: any) {
    this.firebaseService.deleteCity(this.userId, city.id);
  }

  save() {
  debugger;
    this.firebaseService.updateCitry(this.userId, this.city.id, this.city.weather).then(res => {
      console.log("done");
    });
  }
}


