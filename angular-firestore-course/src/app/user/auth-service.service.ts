import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../shared/interfaces/user";
import {AngularFireAuth} from "@angular/fire/auth";
import {MatSnackBar} from "@angular/material";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
  user$: Observable<User> | null;

  constructor(private http: HttpClient, public afAuth: AngularFireAuth, private snackBar: MatSnackBar, private router: Router) {
    this.user$ = this.afAuth.authState;
  }
  logout()
  {
    this.afAuth.auth.signOut().then(()=>{
      this.snackBar.open('Byl jsi odhlášen','OK',{duration: 5000});
      this.router.navigate(['/']);
    });
  }


}

