import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {AgmCoreModule} from '@agm/core';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDKkyWHHchjBQzPNJ0Q78YYG29sQrD62Y0'
    })
  ],
  exports: [
    FormsModule,
    AgmCoreModule
  ]
})
export class SharedModule {
}
