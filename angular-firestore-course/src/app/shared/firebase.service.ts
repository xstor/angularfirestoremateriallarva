import {Injectable} from '@angular/core';
import {AngularFireStorage} from '@angular/fire/storage';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import actions from '@angular/fire/schematics/deploy/actions';
import {map} from 'rxjs/operators';
import {City} from "./interfaces/city";

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  userCollection: AngularFirestoreCollection = this.angularFirestore.collection('users');
  cityCollection: AngularFirestoreCollection;

  constructor(private angularFirestore: AngularFirestore) {
  }

  getUserCities(userId: string): Observable<any[]> {
    this.cityCollection = this.angularFirestore.collection('users/' + userId + '/cities', (ref) => ref.orderBy('time', 'desc'));
    return this.cityCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return {...data};
      })));

  }


  addCity(userId: string, weather: any) {
    const city = {
      weather,
      time: new Date()
    };
    return this.userCollection.doc(userId).collection('cities').add(city);
  }

  getCity(userId: string, cityId: string) {
    return this.angularFirestore.doc('users/' + userId + '/cities/' + cityId);
  }

  updateCitry(userId: string, city: string, weather: any) {
    const newCity = {
      weather,
      time: new Date()
    };
    return this.getCity(userId, city).set(newCity);
  }

  deleteCity(userId: string, city: string) {
    this.angularFirestore.doc('users/' + userId + '/cities/' + city).delete();
  }
}
